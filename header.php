<!DOCTYPE html>
<html>
	<head>
		<title><?php echo bloginfo('name');?></title>
		<meta charset="UTF-8">
		<meta name="description" content="<?php echo bloginfo('description');?>">	
		<meta name="keywords" content="blog, anthony, terrell, developer, photographer, rants, ramblings, technology, life, lessons">	
		<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('stylesheet_url');?>">
	</head>
<body>
	<div class="off-canvas-wrap" data-offcanvas>
	  <div class="inner-wrap">
	     <a class="right-off-canvas-toggle menu-icon" href="#" ><span>Menu</span></a>
	    
		   <aside class="right-off-canvas-menu">
	      <ul class="off-canvas-list">
	        <li><label>Navigation</label></li>
	        <li><a href="#">Categories</a></li>
	        <li><a href="#">Posts</a></li>
	      </ul>
	    </aside>
	    
	  
			<?php if(is_home()): ?>
				<div class="full-width hero">
					<div class="row">
						<div class="large-12 columns">
							<!-- <h1>I don't design SHIT.</h1> -->
							<!-- <h1>Just fucking ramblin'</h1> -->
							<h1>Would rather be brewing beer.</h1>
						</div>
					</div>
					
					
				</div>
			<?php endif; ?>
			
