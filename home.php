<?php 
/**
 * The main blogroll template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage anthonyterrell
 * @since Anthony Terrell 3.0
 */
 get_header(); ?>
 <div class="row">
 	<div class="large-11 columns">
 		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 			<?php if(in_category('photography')): ?>
 				<?php include('post_photography.php'); ?>
 			<?php else: ?>
 				<?php include('post_text.php'); ?>
 			<?php endif; ?>
 			<!-- <div class="row post">
 				<div class="large-3 columns meta hide-for-medium-down">
 					<?php the_time('F jS, Y') ?> 
 					<p><?php the_category(', '); ?></p>
 					<div class="tags">
 						<?php the_tags('', ''); ?>
 					</div>
 				</div>
 			  <div class="large-9 medium-12 small-12 columns">
 				  <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

 				  <div class="entry">
 				    <?php the_content('Read more &#8608;'); ?>
 				  </div>
 			  </div>
 			</div> -->
 		  <?php endwhile; else: ?>
 			  <p>Sorry, no posts matched your criteria.</p>
	  <?php endif; ?>
 	</div>	
 </div>

 <div class="previous">
 	<?php previous_posts_link( 'Previous Posts &#10092;' ); ?>
 </div>
 
 <div class="next">
 	<?php next_posts_link( 'Next Posts &#10093;'); ?>
 </div>
 

<?php get_footer(); ?>