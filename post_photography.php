<div class="row post">
	<div class="large-3 columns meta hide-for-medium-down">
		<?php the_time('F jS, Y') ?> 
		<p><?php the_category(', '); ?></p>
		<div class="tags">
			<?php the_tags('', ''); ?>
		</div>
	</div>
  <div class="large-9 medium-12 small-12 columns">
	  <!-- <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2> -->

	  <div class="entry">
	    <?php the_content('Read more &#8608;'); ?>
	  </div>
  </div>
</div>