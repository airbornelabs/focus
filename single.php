<?php 
/**
 * The single post template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage anthonyterrell
 * @since Anthony Terrell 3.0
 */
 get_header(); ?>
 <div class="row <?php echo (is_home() ? '' : 'single');?>">
 	<div class="large-10 columns left">
 		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 			<div class="row post">
 				<div class="large-3 columns meta">
 					<?php the_time('F jS, Y') ?> 
 					<p>Category <?php the_category(', '); ?></p>
 					<div class="tags">
 						<?php the_tags('', ''); ?>
 					</div>
 				</div>
 			  <div class="large-9 medium-12 small-12 columns">
 				  <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

 				  <div class="entry">
 				    <?php the_content('Read more &#8608;'); ?>
 				  </div>

 				  <div class="comments">
 				  	<?php comments_template(); ?>
 				  </div>
 			  </div>
 			</div>
 		  <?php endwhile; else: ?>
 			  <p>Sorry, no posts matched your criteria.</p>
	  <?php endif; ?>
 	</div>
 </div>

<?php get_footer(); ?>